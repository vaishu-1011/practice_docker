FROM centos:7
RUN yum install httpd -y
RUN echo "<h1> Hello cloud</h1>" > /var/www/html/index.html
CMD httpd -DEFORGROUND
ENTRYPOINT httpd -DEFORGROUND